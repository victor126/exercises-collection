#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float temp;
    
    printf("escribe una temperatura en grados Celcius: ");
    scanf("%f", &temp);
    
    getchar ();
    
    printf("\n%f grados Celcius son: %f grados fahrenheit", temp, (temp*1.8)+32);
    
    getchar ();
    
    return 0;
    
}