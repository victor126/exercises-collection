#include <stdio.h>
#include <stdlib.h>

int IsNegative();

int main(){
    
    int num;
    
    printf("escribe un numero: ");
    scanf("%d",&num);
    
    getchar();
    
    IsNegative(num);
    
    getchar();
    
    return 0;
}
    
int IsNegative(int num){
    
    if(num<0){
        printf("\nel numero %d es negativo.", num);
    }else{
        printf("\nel numero %d es positivo.",num);
    }
    
    return 0;
}