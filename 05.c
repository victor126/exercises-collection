#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float tmp;
    
    printf("escribe el radio de una circumferencia con decimal:");
    scanf("%f", &tmp);
    
    getchar ();
    
    printf("\nel diametro es: %f", tmp * 2);
    
    printf("\n\nel perimetro es: %f", (tmp * 2) * 3.1416);
    
    printf("\n\nel area es: %f", 3.141592 * (tmp * tmp));
    
    
    getchar ();


return 0;

}