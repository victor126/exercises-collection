#include <stdio.h>
#include <stdlib.h>

int main()
{
    
    float s;
    
    printf("escribe un numero de segundos: ");
    scanf("%f", &s);
    
    getchar ();
    
    printf("\n%f segundos son %f minutos", s, s/60);
    
    printf("\n%f segundos son %f horas", s, s/3600);
    
    printf("\n%f segundos son %f dias", s, s/86400);
    
    getchar ();

return 0;

}